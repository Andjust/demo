package com.andreasjusten.demo.service;

import antlr.StringUtils;
import com.andreasjusten.demo.model.Url;
import com.andreasjusten.demo.repository.UrlRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UrlShortenerService {

    @Autowired private UrlRepository urlRepository;

    public String findLongUrl(String shortUrl) throws URISyntaxException {

        final List<Url> urlEntities = (List<Url>) urlRepository.findAll().stream()
                .filter(url -> url.getShortUrl().equalsIgnoreCase(shortUrl))
                .collect(Collectors.toList());

        return urlEntities.size() == 0 ? null :  urlEntities.get(0).getLongUrl();
    }


    public boolean urlInUse(String requestUrl) {

        final List<Url> urlEntities = (List<Url>) urlRepository.findAll().stream()
                .filter(url -> url.getLongUrl().equalsIgnoreCase(requestUrl))
                .collect(Collectors.toList());
        return urlEntities.size() != 0;

    }

    public String createShortUrl(String longUrl) {

        Url url = new Url();
        url.setLongUrl(longUrl);
        Url entity = urlRepository.save(url);

        return url.getShortUrl();
    }
}
