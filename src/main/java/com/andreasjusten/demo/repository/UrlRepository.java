package com.andreasjusten.demo.repository;

import com.andreasjusten.demo.model.Url;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UrlRepository extends JpaRepository<Url, Long>{
}
