package com.andreasjusten.demo.controller;

import com.andreasjusten.demo.model.Url;
import com.andreasjusten.demo.service.UrlShortenerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

@RestController
@RequestMapping("/")
public class UrlShortenerController {

    @Autowired private UrlShortenerService urlShortenerService;

    @GetMapping(value = ("/redirect"))
    public ResponseEntity<Void> redirectToLongUrl(
            @RequestParam(name = "url", required = true) final String url)
            throws URISyntaxException {

        final String responseUrl = urlShortenerService.findLongUrl(url);
        if (responseUrl == null)  {
            return new ResponseEntity(
                    "operation not possible - url undefined ",
                    HttpStatus.NOT_FOUND);
        }

        return ResponseEntity.status(HttpStatus.FOUND).location(URI.create(responseUrl)).build();
    }

    @PostMapping("/shorten")
    public ResponseEntity<String> createShortUrl(
            @RequestParam(name = "url", required = true) final String url) {

        if (urlShortenerService.urlInUse(url)) {
            return new ResponseEntity(
                    "operation not possible - url is already in use ",
                    HttpStatus.FORBIDDEN);
        }
        final String shortUrl = urlShortenerService.createShortUrl(url);
        return new ResponseEntity(shortUrl, HttpStatus.OK);
    }
}