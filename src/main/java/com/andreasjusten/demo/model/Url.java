package com.andreasjusten.demo.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "url")
public class Url {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String longUrl;
    private String shortUrl;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLongUrl() {
        return longUrl;
    }

    public void setLongUrl(String longUrl) {
        this.longUrl = longUrl;
        String hashString = Integer.toString(longUrl.hashCode());
        this.shortUrl = hashString.substring(hashString.length()-6);
    }

    public String getShortUrl() {
        return shortUrl;
    }

}
